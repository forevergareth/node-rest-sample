const mongoose = require('mongoose')

var User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 9
  }, password: {
    type: String,
    required: true
  }
})

module.exports = {User}