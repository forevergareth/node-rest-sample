const MongoClient = require('mongodb').MongoClient

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to Mongo Server')

  }
  console.log('Connected to Mongo Database Server')
  // db.collection('Todos').insertOne({
  //   text: 'Something to do',
  //   completed: true
  // }, (err, res) => {
  //   if (err) {
  //     return console.log('Unable to insert item', err)
  //   }

  //   console.log(JSON.stringify(res.ops,undefined,2))
  // })

  db.collection('Users').insertOne({
    name: 'Gareth',
    age: 18,
    location: 'Mammee Bay'
  }, (err, res) => {
    if (err) {
      return console.log('Unable to insert item', err)
    }
    console.log(JSON.stringify(res.ops,undefined,2))
  })
  db.close();
})