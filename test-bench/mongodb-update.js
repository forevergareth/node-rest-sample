const {MongoClient, ObjectID} = require('mongodb')

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to Mongo Server')
  }
  console.log('Connected to Mongo Database Server')

 
  //findOneAndUpdate
  db.collection('Todos').findOneAndUpdate({
    _id: new ObjectID('59af294f3268ae4477b6cf99')
  }, {
      $set: {
        completed: true
      }
    }, {
      returnOriginal: false
    })
    .then((result) => {
      console.log(result);
    });
  //db.close();
})