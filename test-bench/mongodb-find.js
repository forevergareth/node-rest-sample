const MongoClient = require('mongodb').MongoClient

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to Mongo Server')
  }
  console.log('Connected to Mongo Database Server')

  db.collection('Todos').find({completed: false}).toArray().then((docs) => {
    console.log('Todos')
    console.log(JSON.stringify(docs,undefined,2))
  }, (err) => {
    console.log('Unable to fetch todos', err)
  })

  //db.close();
})