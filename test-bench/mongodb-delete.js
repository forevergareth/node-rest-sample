const MongoClient = require('mongodb').MongoClient

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to Mongo Server')
  }
  console.log('Connected to Mongo Database Server')

  //deleteMany
  // db.collection('Todos').deleteMany({ text: 'Write that book' }).then((result) => {
  //   console.log(result)
  // })
  
  //deleteOne
  // db.collection('Todos').deleteOne({ text: 'Get Food' }).then((result) => {
  //   console.log(result)
  // })

  //findAndDeleteOne
  db.collection('Todos').findOneAndDelete({ completed: true }).then((result) => {
    console.log(result)
  })
  //db.close();
})